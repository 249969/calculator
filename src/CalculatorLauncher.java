public class CalculatorLauncher {
    public static void main(String[] args) {
        System.out.println(dodawanie(2,3));
        System.out.println(odejmowanie(2,3));
        System.out.println(mnozenie(2,3));
        System.out.println(dzielenie(2,3));
        System.out.println(potegowanie(2,3));
}

    public static int dodawanie(int a, int b){
        return a+b;
    }
    public static int odejmowanie(int a, int b){
        return a-b;
    }
    public static int mnozenie(int a, int b){
        return a*b;
    }
    public static double dzielenie(int a, int b){
        if (b==0)
            return 0;
        return ((double)a / (double)b);
    }
    public static int potegowanie(int a, int b){
        return (int) Math.pow(a,b);
    }
}
